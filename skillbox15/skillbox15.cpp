﻿#include <iostream>

void FindOddNumber(int N)
{
    for (int i = 0; i <= N; i++)
    {
        if (i % 2 == 1)
        {
            std::cout << i << std::endl;
        }
    }
}

int main()
{
    int a;
    std::cin >> a;
    FindOddNumber(a);
}